package solveraapps.testwebviewoncustomview.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class SimpliestView extends View {

    WebView webView;

    public SimpliestView(Context context) {
        super(context);
        webView = new WebView(context);
        webView.setBackgroundColor(Color.RED);
        webView.loadData("This text I would like to show at start !","text/html", "utf-8");
        webView.setLayoutParams(new LinearLayout.LayoutParams(1000,1000));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        webView.draw(canvas);
    }
}
