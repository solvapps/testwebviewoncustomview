package solveraapps.testwebviewoncustomview.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import solveraapps.testwebviewoncustomview.MainActivity;
import solveraapps.testwebviewoncustomview.backgroundloader.LoadEventType;

public class MapView extends View implements GestureDetector.OnGestureListener{

    private GestureDetector gestureDetector;
    MainActivity mainActivity;


    Paint myPaint = new Paint();

    public MapView(MainActivity mainActivity) {
        super(mainActivity);
        this.mainActivity = mainActivity;
        gestureDetector = new GestureDetector(this);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//         Sample drawing stuff.
        myPaint.setColor(Color.BLUE);
        canvas.drawPaint(myPaint);
        myPaint.setColor(Color.YELLOW);
        canvas.drawCircle(getWidth() / 2.f, getHeight() / 2.f, 200, myPaint);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
//        infoBox.dispatchGenericMotionEvent(motionEvent);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
//        infoBox.dispatchGenericMotionEvent(motionEvent);

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        mainActivity.mapClickedWithEvent(motionEvent, LoadEventType.CLICKED);
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return true;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.invalidate();
        return gestureDetector.onTouchEvent(event);
    }



}
