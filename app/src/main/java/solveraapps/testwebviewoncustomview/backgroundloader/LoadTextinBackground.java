package solveraapps.testwebviewoncustomview.backgroundloader;

import android.os.AsyncTask;

import solveraapps.testwebviewoncustomview.MainActivity;

/**
 * This is for loading texts in the background.
 */
public class LoadTextinBackground extends AsyncTask<Void, Void, String> {

	private final LoadEventType loadEventType;
	private MainActivity mainActivity;

	public LoadTextinBackground(MainActivity mainActivity, LoadEventType loadEventType) {
		this.mainActivity = mainActivity;
		this.loadEventType = loadEventType;
	}


	@Override
	protected String doInBackground(Void... voids) {

		if(LoadEventType.CLICKED == loadEventType){
			return clickedText();
		}else if(LoadEventType.INIT == loadEventType){
			return initText();
		}else{
			return "";
		}
	}

	@Override
	protected void onPostExecute(String text) {
		super.onPostExecute("");
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}

	private String initText(){
		return "<html>\n" +
				"<head>\n" +
				"<title>\n" +
				"This is the first Text, which should be shown from the beginning, but the window is just empty after start !\n" +
				"</title>\n" +
				"</head>\n" +
				"<body>\n" +
				"<b>not clicked yet !</b>\n" +
				bigBlabla() +
				"</body>\n" +
				"</html>";
	}

	private String clickedText(){
		return "<html>\n" +
				"<head>\n" +
				"<title>\n" +
				"A Simple HTML Document\n" +
				"</title>\n" +
				"</head>\n" +
				"<body>\n" +
				"<b>I have clicked, but it does show only after the second click. I hate it :-) !</b>\n" +
				bigBlabla() +
				"</body>\n" +
				"</html>";
	}


	public String bigBlabla(){
		return "This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n" +
				"This is a line of text. Bla bla bla\n";
	}

}
