package solveraapps.testwebviewoncustomview.backgroundloader;

public enum LoadEventType {

    INIT, CLICKED
}
