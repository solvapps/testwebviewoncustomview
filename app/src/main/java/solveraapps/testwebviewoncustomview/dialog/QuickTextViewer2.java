package solveraapps.testwebviewoncustomview.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * Created by andreas on 02.12.2017.
 */

public class QuickTextViewer2 {

    WebView webview;
    Context context;

    Dialog  dialog;

    AlertDialog.Builder alert;


    public QuickTextViewer2(Context context){
        this.context = context;
        webview = new WebView(context);
        webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.loadData("No Text", "text/html", "utf-8");
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });


        webview.setBackgroundColor(Color.YELLOW);
        alert.setView(webview);


        dialog = new Dialog(context);
//        dialog.setContentView(R.layout);


        Window window = dialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.RIGHT);
        dialog.getWindow().setGravity(Gravity.END);

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    public void showDialog(){
        dialog.show();
        dialog.getWindow().setLayout(900, 400);
    }

    public void setSize(int w, int h){
        dialog.getWindow().setLayout(w, h);
    }

    public void setPosition(float x, float y){
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = (int) x;
        wmlp.y = (int) y;
    }

    public void setText(String text){
        webview.loadData(text, "text/html", "utf-8");

    }

    public static int getWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public void showDialog_old(){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);

        mBuilder.setTitle("Textviewer");
        mBuilder.setCancelable(true);

        mBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.v("Canceled","canceled !");
            }
        });

        mBuilder.setView(webview);
        final AlertDialog mDialog = mBuilder.create();


        Window window = mDialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setLayout(200,200);
        window.setGravity(Gravity.TOP);

        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);


        mDialog.show();
    }

    /**
     * Sets all text for specific language.
     */
    public void assignLabelTexts(){

    }

    /**
     * Assigns all UI Objects to Layoutobjects from mapoptiondialog.xml
     */
    public void assignResources(){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

//        mView = context.getLayoutInflater().inflate(R.layout.mapoptiondialog,null);
//        mView = inflater.inflate(R.layout.mapoptiondialog,null);

//        tvlabelShowEvents = (TextView) mView.findViewById(R.id.labelShowEvents);
   }




}