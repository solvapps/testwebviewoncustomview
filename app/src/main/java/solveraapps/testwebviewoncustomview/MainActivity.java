package solveraapps.testwebviewoncustomview;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import solveraapps.testwebviewoncustomview.backgroundloader.LoadEventType;
import solveraapps.testwebviewoncustomview.backgroundloader.LoadTextinBackground;
import solveraapps.testwebviewoncustomview.dialog.QuickTextViewer;
import solveraapps.testwebviewoncustomview.helpers.BoxPositionCalculator;
import solveraapps.testwebviewoncustomview.layouts.TouchEventView;
import solveraapps.testwebviewoncustomview.views.MapView;
import solveraapps.testwebviewoncustomview.views.SimpliestView;

public class MainActivity extends AppCompatActivity {

    TouchEventView layout;
    MapView mapView;
    SimpliestView simpliestView;

    int width;
    int height;

    //TextViewer textViewer;
    QuickTextViewer textViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        setupScreens();
        layout.setWillNotDraw(false);

        setContentView(layout);

        //textViewer = new TextViewer(this);
        textViewer = new QuickTextViewer(this);
        textViewer.showDialog();
    }


    public void mapClickedWithEvent(MotionEvent motionEvent, LoadEventType loadEventType){
        Rect rect = BoxPositionCalculator.getPosition(motionEvent.getX(), motionEvent.getY(), this);
        LoadTextinBackground loadTextinBackground = new LoadTextinBackground(this, loadEventType);
        loadTextinBackground.execute();
        textViewer.setSize(500,500);
        textViewer.setPosition(motionEvent.getX(),motionEvent.getY());
        textViewer.setText("Map clicked ! + " + motionEvent.getX() + ":" + motionEvent.getY());


    }

    public void setupScreens(){
        mapView = new MapView(this );
        layout = new TouchEventView(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(mapView);

        LoadTextinBackground loadTextinBackground = new LoadTextinBackground(this, LoadEventType.INIT);
        loadTextinBackground.execute();


    }

    public void invalidate(){
        mapView.invalidate();
    }


}
