package solveraapps.testwebviewoncustomview.layouts;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import solveraapps.testwebviewoncustomview.MainActivity;

public class TouchEventView extends LinearLayout {

    MainActivity mainActivity;

    public TouchEventView(MainActivity mainActivity) {
        super(mainActivity, null);
        this.mainActivity = mainActivity;
        init();
    }

    private void init() {
        Button button = new Button(getContext());
        button.setText("refresh Screen !");
        button.setLayoutParams(new LayoutParams(400, 100));
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.invalidate();
            }
        });
        addView(button);
    }

}