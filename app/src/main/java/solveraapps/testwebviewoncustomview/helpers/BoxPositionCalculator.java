package solveraapps.testwebviewoncustomview.helpers;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.WindowManager;

public class BoxPositionCalculator {


    public static final int ABSTANDRAND = 100;

    public static Rect getPosition(float mousex, float mousey, Context context){

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        boolean isright = mousex > width / 2;
        boolean isup = mousey < height / 2;

        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            // code to do for Portrait Mode
            if(isup){
                return new Rect(ABSTANDRAND, height/2 + ABSTANDRAND, width- ABSTANDRAND, height - ABSTANDRAND);
            }else{
                return new Rect(ABSTANDRAND, ABSTANDRAND, width- ABSTANDRAND, height/2-ABSTANDRAND);
            }

        } else {
            // code to do for Landscape Mode
            if(isright){
                return new Rect(ABSTANDRAND, ABSTANDRAND, width/2- ABSTANDRAND, height-ABSTANDRAND);
            }else{
                return new Rect(width / 2 + ABSTANDRAND,  ABSTANDRAND, width- ABSTANDRAND, height - ABSTANDRAND);
            }
        }
    }
}
